# Load balancer client
This project contains a load balancer client for communicating with the corresponding load balancer's API. It requires
a Java SE 11 JRE to run. The client should only be started after the load balancer has started, because it will
currently exit if no load balancer is available. The application takes the following command line arguments.

1. Load balancer host name, which can be either an IP-address or a domain name
2. Load balancer port number
3. Log file path  
  
How to get jar to VM:  
scp /path/to/file/on/ComputerB ComputerAUser@ComputerA:/path/to/dump/file/on/ComputerA  

How to automatically run jar file on VM:  
1. create shell script 
```
  #!/bin/bash
  java -jar <absolute path to jar file> <local IP of load balancer (not public IP)> <TCP port load balancer is listening on> <absolute path to log file (folder must exist, file does not have to)>
```
2. crontab -e
3. enter @reboot /absolute_path_to_shell_script  
  
Now java file will start on reboot.
