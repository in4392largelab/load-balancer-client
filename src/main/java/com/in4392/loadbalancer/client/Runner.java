package com.in4392.loadbalancer.client;

import org.apache.commons.io.output.TeeOutputStream;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

class Runner {

    /**
     *
     * @param args args[0]: load balancer host; args[1]: load balancer port; args[2]: log file path
     */
    public static void main(String[] args) {
        FileOutputStream fileOutputStream;
        try {
            fileOutputStream = new FileOutputStream(args[2], true);
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        }
        TeeOutputStream teeOutputStream = new TeeOutputStream(System.out, fileOutputStream);
        System.setOut(new PrintStream(teeOutputStream));

        System.out.println("Starting load balancing client");
        LoadBalancerClient client = new LoadBalancerClient(args[0], Integer.parseInt(args[1]));

        new Thread(() -> {
            try {
                client.start();
            } catch (LoadBalancerCommunicationException e) {
                System.out.println("Communication with load balancer failed. Exiting.");
            }
        }).start();

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                client.stop();
            } catch (LoadBalancerCommunicationException e) {
                System.out.println("Could not signal exit to load balancer. Exiting anyway.");
            }
        }));
    }
}
