package com.in4392.loadbalancer.client;

import java.io.IOException;

/**
 * Exception thrown when communication with the load balancer fails
 */
class LoadBalancerCommunicationException extends IOException {
    LoadBalancerCommunicationException() {
    }

    LoadBalancerCommunicationException(String message) {
        super(message);
    }

    LoadBalancerCommunicationException(String message, Throwable cause) {
        super(message, cause);
    }

    LoadBalancerCommunicationException(Throwable cause) {
        super(cause);
    }
}
