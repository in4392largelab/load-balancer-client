package com.in4392.loadbalancer.client;

import java.io.IOException;
import java.net.*;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

/**
 * Load balancer client that announces its presence to the load balancer, so that it receives tasks from clients.
 */
class LoadBalancerClient {
    private final String loadBalancerHost;
    private final int loadBalancerPort;

    LoadBalancerClient(String loadBalancerHost, int loadBalancerPort) {
        this.loadBalancerHost = loadBalancerHost;
        this.loadBalancerPort = loadBalancerPort;
    }

    /**
     * Starts the load balancer client by announcing itself to the load balancer
     * @throws LoadBalancerCommunicationException when communication with the load balancer fails
     */
    void start() throws LoadBalancerCommunicationException {
        try {
            sendHeartBeat();
            Thread.sleep(10_000L);

            //noinspection InfiniteLoopStatement
            while(true) {
                sendHeartBeat();
                Thread.sleep(15_000L);
                if (!activeOnLoadBalancer()) {
                    throw new LoadBalancerCommunicationException();
                }
            }
        } catch (IOException e) {
            throw new LoadBalancerCommunicationException(e);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    /**
     * Stops the load balancer client by announcing this to the load balancer
     * @throws LoadBalancerCommunicationException when communication with the load balancer fails
     */
    void stop() throws LoadBalancerCommunicationException {
        try {
            sendDeleteRequest();
        } catch (IOException e) {
            throw new LoadBalancerCommunicationException(e);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    /**
     * Sends a request to the load balancer to delete this server from the list of active servers
     */
    private void sendDeleteRequest() throws IOException, InterruptedException {
        System.out.println("Sending delete request to load balancer");

        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://" + loadBalancerHost + ":" + loadBalancerPort + "/server"))
                .DELETE().build();
        client.send(request, HttpResponse.BodyHandlers.discarding());
    }

    /**
     * Checks with the load balancer whether this server is indicated as active
     */
    private boolean activeOnLoadBalancer() throws IOException, InterruptedException {
        System.out.println("Checking if load balancer acknowledges presence of this machine");

        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://" + loadBalancerHost + ":" + loadBalancerPort + "/server"))
                .GET().build();
        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
        return response.body().equals("active");
    }

    /**
     * Sends a heart beat to the load balancer to indicate that this server is still running
     */
    private void sendHeartBeat() throws IOException, InterruptedException {
        System.out.println("Sending heart beat to load balancer");

        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://" + loadBalancerHost + ":" + loadBalancerPort + "/server"))
                .POST(HttpRequest.BodyPublishers.noBody()).build();
        client.send(request, HttpResponse.BodyHandlers.discarding());
    }
}
